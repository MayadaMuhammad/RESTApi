import { Meteor } from 'meteor/meteor';
import { Restivus } from 'meteor/nimble:restivus'

Items = new Mongo.Collection('items');
Articles = new Mongo.Collection('articles');

if (Meteor.isServer) {

  // Global API configuration
  var Api = new Restivus({
    useDefaultAuth: true,
    prettyJson: true
  });

  // Generates: GET, POST on /api/items and GET, PUT, PATCH, DELETE on
  // /api/items/:id for the Items collection
  Api.addCollection(Items);
  //Api.addCollection(Articles);
  //Api.addCollection(Meteor.users);

  Api.addRoute('articles', {authRequired: true}, {
    get: {
      authRequired: false,
      action: function () {
        // GET api/articles
        console.log('Testing manual response');
        this.response.write('This is a manual response');
        this.done();  // Must call this immediately before return!
      }
    },
    post: function () {
      // POST api/articles
    },
    put: function () {
      // PUT api/articles
    },
    patch: function () {
      // PATCH api/articles
    },
    delete: function () {
      // DELETE api/articles
    },
    options: function () {
      // OPTIONS api/articles
    }
  });

  // Generates: POST on /api/users and GET, DELETE /api/users/:id for
  // Meteor.users collection
  /*Api.addCollection(Meteor.users, {
    excludedEndpoints: ['getAll', 'put'],
    routeOptions: {
      authRequired: true
    },
    endpoints: {
      post: {
        authRequired: false
      },
      delete: {
        roleRequired: 'admin'
      }
    }
  });*/

  // Maps to: /api/articles/:id
  /*Api.addRoute('articles/:id', {authRequired: true}, {
    get: function () {
      return Articles.findOne(this.urlParams.id);
    },
    delete: {
      roleRequired: ['author', 'admin'],
      action: function () {
        if (Articles.remove(this.urlParams.id)) {
          return {status: 'success', data: {message: 'Article removed'}};
        }
        return {
          statusCode: 404,
          body: {status: 'fail', message: 'Article not found'}
        };
      }
    }
  });*/
}
